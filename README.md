# motorPhytron
EPICS motor drivers for the following [Phytron GmbH](https://www.phytron.eu/) controllers: I1AM01 Stepper Motor Controller

[![Build Status](https://github.com/epics-motor/motorPhytron/actions/workflows/ci-scripts-build.yml/badge.svg)](https://github.com/epics-motor/motorPhytron/actions/workflows/ci-scripts-build.yml)
<!--[![Build Status](https://travis-ci.org/epics-motor/motorPhytron.png)](https://travis-ci.org/epics-motor/motorPhytron)-->

motorPhytron is a submodule of [motor](https://github.com/epics-modules/motor).  When motorPhytron is built in the ``motor/modules`` directory, no manual configuration is needed.

motorPhytron can also be built outside of motor by copying it's ``EXAMPLE_RELEASE.local`` file to ``RELEASE.local`` and defining the paths to ``MOTOR`` and itself.

motorPhytron contains an example IOC that is built if ``CONFIG_SITE.local`` sets ``BUILD_IOCS = YES``.  The example IOC can be built outside of driver module.

## Use in a Container

The IOC in this support module has been modified to allow for it to be used inside a generic image. The IOC is configured to not restart at boot. The folling environment variables are expected

`IOC_SYS`
`IOC_DEV`
`IOC_IP`

In the example two channels are instantiated. If you want more you can modify the files:

`motor.substituitons.phytron`
`motorPhyton.cmd`
`settings.req`

## A note on autosave

Autosave is added only for the PV's that are included in the motor record. This allows for PV's for the Phyton controller itself to be defined only in the substitutions file and rewritten when the reset PV is asserted
